const Task = require("../models/taskModel");

// CREATE/ POST DATA CONTROLLER [SECTION] or create a task
const  createTask = async (req, res) => {
    try {
        const createTask =  await Task.create(req.body);
        res.status(200).json(createTask);
    } catch (error) {
        res.status(404).json({message: error.message})
    }  

}

// GET/RETRIVE DATA CONTROLLER SECTION or get all task 
const  getTask = async (req, res) => {
    try {
        const getalltasks =  await Task.find();
        res.status(200).json(getalltasks);
    } catch (error) {
        res.status(404).json({message: error.message})
    }  

}

//GET BY ID 
const getTaskbyId = async (req, res) => {
    try {
        const {id} = req.params
        const task =  await Task.findById(id);
        // if the id is not exist
        if (!task) {
            return res.status(404).json(`NO task with id ${id}`);
        }
        // console.log(task)
        res.status(200).json(task)
    } catch (error) {
        res.status(404).json({message: error.message})
    }
}


//DELLETE TASK BY ID
const deleteTask =  async (req,res) => {
    // console.log(req.params)
    try {
          const {id}  = req.params
          const task  = await Task.findByIdAndDelete(id);
             // if the id is not exist
          if (!task) {
            return res.status(404).json(`NO task with id ${id}`);
        }
          res.status(200).send("Task Delete" +  task);

    } catch (error) {
        res.status(404).json({message: error.message})
    } 

};

// UPDATE DATA OR TASK BY ID

const updateTask = async (req,res) => {
    try {
        const {id} = req.params;
        const task = await Task.findByIdAndUpdate(
            {_id: id }, req.body, {new:true, 
            runValidators: true}
        );
        //if the  id is did not exist 
        if (!task) {
            return res.status(404).json(`NO task with id ${id}`);
        } 
        
        console.log(task);
            res.status(200).json(task)
    } catch (error) {
        res.status(404).json({message: error.message})
    }

}

// list of callback function which exports for route
module.exports = {
        createTask,
        getTask,
        getTaskbyId,
        deleteTask,
        updateTask
}