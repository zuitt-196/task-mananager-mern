const express = require('express');
const { createTask, getTask,getTaskbyId,deleteTask,updateTask } = require('../controllers/taskController');
const router = express.Router();





// router.route("/").get(getTask).post(createTask)
// router.route("/:id").get(getTaskbyId).delete(deleteTask).put(updateTask)


// there have two option how to  put the router shotrt hand and individual


// CREATE  POST OR TASK ROUTE 
router.post("/", createTask);

//GET/READ Data ROUTE
router.get("/" , getTask);

//GET/READ DATA BY ID ROUTE
router.get("/:id" , getTaskbyId);

// DELETE THE DATA ROUTE
router.delete("/:id" ,deleteTask);

// UPDATE DATA OR TASK ROUTE
router.put("/:id" ,updateTask);

module.exports = router
